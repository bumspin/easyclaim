// JavaScript Document

(function(){
	"use strict";
	
	var navParent = $(".navigation-parent"),
		//bodytag = $("#home"),
		navAnchor = $("#nav-sect .nav>li>a"),
		iconclose = $(".icon-close");

	iconclose.click(function(){
		navParent.removeClass("in");
	});
	navAnchor.click(function(){
		navParent.removeClass("in");
	});

	$(function() {
	  $('.navigation li a[href*="#"]:not([href="#"]), .hero-txt a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 1000);
			return false;
		  }
		}
	  });
	});
}());
