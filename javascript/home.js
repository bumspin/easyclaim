// JavaScript Document


var doc = $(window),
	header = $(".header"),
	navColor = $(".navbar-default .navbar-nav>li>a"),
	makeClaim = $(".makeclaim"),
	burgerWhite = $(".burger-white"),
	burgerGreen = $(".burger-green"),
	navParent = $(".navigation-parent"),
	bodytag = $("#home"),
	navAnchor = $("#nav-sect .nav>li>a"),
	claimBtn = $(".claim-btn-wrap"),
	scrolPad = $(".how-to");
	
doc.scroll(function(){
	var windowpos = doc.scrollTop(),
		windowWidth = $(window).innerWidth();
		
	if(windowpos >= 5){
		burgerWhite.addClass("hide-elem");
		burgerGreen.removeClass("hide-elem");
		header.addClass("header-bg-second");
		header.removeClass("header-bg-first");
		navColor.css({
			"color": "#177556"
		});
		makeClaim.css({
			"color": "#fff"
		});
	}
	else{
		header.addClass("header-bg-first");
		header.removeClass("header-bg-second");
		navColor.css({
			"color": "#fff"
		});
		burgerWhite.removeClass("hide-elem");
		burgerGreen.addClass("hide-elem");
	}
		
	if(windowWidth <= 767){
		if(windowpos >= 630){
			header.removeClass("nav-fix");
			header.addClass("nav-non-fix");
		}
		else{
			header.removeClass("nav-non-fix");
			header.addClass("nav-fix");
			/*header.css('top', '0');*/
		}
		if(windowpos >= 700){
			claimBtn.addClass("claim-fix");
			scrolPad.addClass("mob-pad");
		}
		else{
			claimBtn.removeClass("claim-fix");
			scrolPad.removeClass("mob-pad");
		}
	}
});
